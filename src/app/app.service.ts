import { AuthService } from 'angular-sso-auth-module';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



@Injectable()
export class AppService {
	constructor(
		private http: Http,
		private authenticationService: AuthService) {
	}

	getUsers(): Observable<any[]> {
		// add authorization header with jwt toke
		const headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
		const options = new RequestOptions({ headers: headers });

		// get users from api
		return this.http.get('http://localhost:8091/get', options)
			.map((response: Response) => response.json());
	}


	decode(): Observable<any[]> {
		return this.http.post('http://localhost:8091/decode', { token: this.authenticationService.token })
			.map((response: Response) => response.json());
	}
}
