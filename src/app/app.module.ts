import { AuthService } from 'angular-sso-auth-module';
import { HttpModule } from '@angular/http';
import { Route, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppService } from './app.service';



export const appRoutes: Route[] = [
	{ path: 'auth', loadChildren: 'angular-sso-auth-module#AuthModule' },
	// { path: 'login', loadChildren: './modules/login/login.module#LoginModule'}
];

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(appRoutes),
		HttpModule
	],
	bootstrap: [AppComponent],
	providers: [AuthService]
})
export class AppModule {
	constructor(private authService: AuthService) {
		this.authService.ssoUrl = 'https://localhost:443';
	}
}
